
API criada para o aplicativo desenvolvido para a disciplina de PDM do curso de engenharia da computação do IFTM.

Passos para utilização.

1. Tenha o Node.js instalado no computador;
2. Tenha o Mysql instalado tambem no computador;
3. Rode o comando npm i dentro da pasta MIAUDOTA para instalação das bibliotecas utlizadas pelas API;
4. Crie um banco de dados com nome qualquer 
5. Crie ainda na pasta MIAUDOTA crie um arquivo com o nome de .env contendo as seguintes informações do Banco:

DB_CONNECTION = mysql
DB_HOST = localhost
DB_USER = root
DB_PASS = 92495145
DB_NAME = testando ==> nome criado no passo 4
DB_PORT = 3306
ENTITIES = src/models/*.ts
MIGRATIONS = src/database/migrations/*.ts
MIGRATIONS_DIR = .src/database/migrations

Preenchendo com os dados do banco e nome do mesmo

6. Para criar as tabelas rode o comando npm run typeorm:run
7. para iniciar a API basta rodar o comando npm run dev:server; 
