import MyError from "../errors"

function ValidaEmail(email: string) {
  const regexEmail = /^[a-z0-9.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i
  if (! regexEmail.test(email)){
    throw new MyError('O e-mail informado é inválido')
  }
}

export default ValidaEmail
