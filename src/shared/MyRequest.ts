import { Request } from 'express'
import Usuario from '../models/Usuario'

export default interface MyRequest extends Request {
  usuario?: Usuario
}