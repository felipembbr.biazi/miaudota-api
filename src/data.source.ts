import 'dotenv/config'
import 'reflect-metadata'
import { DataSource } from 'typeorm'

const port = process.env.DB_PORT as number | undefined
const entities = process.env.ENTITIES as string
const migrations = process.env.MIGRATIONS as string

export const ApiDataSource = new DataSource ({
    type: 'mysql',
    host: process.env.DB_HOST,
    port: port,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    entities: [entities],
    migrations: [migrations],
})