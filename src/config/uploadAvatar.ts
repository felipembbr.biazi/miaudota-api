import crypto from 'crypto'
import aws from 'aws-sdk'
import multers3 from 'multer-s3'

const bucket = 'miaudota/avatars'
const accessKeyId = 'AKIA3A67CFCXZP2LQ34X'
const secretAccessKey = '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'
const region = 'sa-east-1'

const s3Config = new aws.S3({
  accessKeyId,
  secretAccessKey,
  region,
  signatureVersion: 'v4'
});

export default {
  storage: multers3({
    s3: s3Config,
    bucket,
    contentType: multers3.AUTO_CONTENT_TYPE,
    metadata: (req, file, cb) => {
      cb(null, {fieldName: file.fieldname})
    },
    key: (req, file, cb) => {
      const filehash = crypto.randomBytes(10).toString('hex')
      const filename = `${filehash}-${file.originalname}`
      cb(null, filename)
    }
  })
}