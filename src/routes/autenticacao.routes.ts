import { Router,Request,Response } from 'express'
import AutenticaUsuarioService from '../services/autenticacao/AutenticaUsuarioService'
import GravaUsuarioService from '../services/usuarios/GravaUsuarioService'
import MyRequest from '../shared/MyRequest'
import multer from 'multer'
import config from "../config/uploadAvatar"

const upload = multer(config)

const autenticacaoRoutes = Router()

autenticacaoRoutes.post('/Register', upload.single('avatar'), async (req: MyRequest, res: Response) => {
  const avatar = req.file
  const { nome,tipo,endereco,telefone,email,senha } = req.body
 
  const gravaUsuarioService = new GravaUsuarioService()

  const usuario = await gravaUsuarioService.execute({
    nome,tipo,endereco,telefone,email,senha,avatar
  })

  return res.status(200).json({ sucesso: true, mensagem: 'Usuário cadastrado com Sucesso' })
})

autenticacaoRoutes.post('/', async (req: Request, res: Response) => {
  const { email,senha } = req.body

  const autenticaUsuarioService = new AutenticaUsuarioService()

  const { usuario,token } = await autenticaUsuarioService.execute({
    email,
    senha
  }) 

  return res.status(200).json({
    sucesso: true,
    usuario,
    token
  })
})

export default autenticacaoRoutes
