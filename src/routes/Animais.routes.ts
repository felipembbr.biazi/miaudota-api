import { Response, Router } from "express";
import multer from "multer";
import config from "../config/uploadAvatar"
import BuscaAnimaisService from "../services/animais/BuscaAnimaisService";
import BuscaAnimalPorIdService from "../services/animais/BuscaAnimalPorIdService";
import BuscaAnimalPorIdUsuarioService from "../services/animais/BuscaAnimalPorIdUsuarioService";
import GravaAnimalService from "../services/animais/GravaAnimalService";
import MyRequest from "../shared/MyRequest";

const upload = multer(config)

const animaisRoutes = Router()

animaisRoutes.get('/', async (req: MyRequest, res: Response) => {
  const buscaAnimaisService = new BuscaAnimaisService()
  const animais = await buscaAnimaisService.execute()

  return res.status(200).json({ sucesso: true,animais })
})

animaisRoutes.get('/:id', async (req: MyRequest, res: Response) => {
  const { id } = req.params
  const buscaAnimaisPorIdService = new BuscaAnimalPorIdService()
  const animal = await buscaAnimaisPorIdService.execute(parseInt(id))

  return res.status(200).json({ sucesso: true,animal })
})

animaisRoutes.get('/PorIdUsuario/:id', async (req: MyRequest, res: Response) => {
  const { id } = req.params
  const buscaAnimalPorIdUsuarioService = new BuscaAnimalPorIdUsuarioService()
  const animais = await buscaAnimalPorIdUsuarioService.execute(parseInt(id))

  return res.status(200).json({ sucesso: true,animais })
})

animaisRoutes.post('/', upload.single('avatar'), async(req: MyRequest, res: Response) => {
  const avatar = req.file
  const { nome,sexo,status,idade,descricao } = req.body

  const gravaAnimalService = new GravaAnimalService()
  const animal = gravaAnimalService.execute({
    nome,sexo,status,idade,avatar,descricao
  })

  return res.status(200).json({ sucesso: true, animal: animal })
})

export default animaisRoutes