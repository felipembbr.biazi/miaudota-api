import { Router,Response } from 'express'
import BuscaUsuarioPorIdService from '../services/usuarios/BuscaUsuarioPorIdService'
import GravaUsuarioService from '../services/usuarios/GravaUsuarioService'
import MyRequest from '../shared/MyRequest'
import multer from 'multer'
import config from "../config/uploadAvatar"
import AlteraUsuarioService from '../services/usuarios/AlteraUsuarioService'
import AlteraAvatarUsuarioService from '../services/usuarios/AlteraAvatarUsuarioService'
import RemoveAvatarUsuarioService from '../services/usuarios/RemoveAvatarUsuarioService'


const upload = multer(config)

const usuariosRoutes = Router()

usuariosRoutes.get('/:id', async (req: MyRequest, res: Response) => {
  const { id } = req.params
  const buscaUsuarioPorIdService = new BuscaUsuarioPorIdService()
  const usuario = await buscaUsuarioPorIdService.execute(parseInt(id))

  return res.status(200).json({ sucesso: true, usuario })
})

usuariosRoutes.put('/:id', async (req: MyRequest, res: Response) => {
  const { id } = req.params
  const { nome,tipo,telefone,endereco} = req.body
  let user = req.usuario

  const alteraUsuarioService = new AlteraUsuarioService()

  const usuario = await alteraUsuarioService.execute({
    id: parseInt(id),nome,tipo,telefone,endereco,usuario: user
  })

  return res.status(200).json({ sucesso: true, usuario: usuario })
})

usuariosRoutes.put('/AlteraAvatar/:id', upload.single('avatar'), async (req: MyRequest, res: Response) => {
	const { id } = req.params
	const imagem = req.file

	const alteraAvatarUsuarioService = new AlteraAvatarUsuarioService()

	const avatarUrl = await alteraAvatarUsuarioService.execute({
		id: parseInt(id),imagem
	})

	return res.status(200).json({ sucesso: true, avatar: avatarUrl })
})

usuariosRoutes.delete('/RemoveAvatar/:id', async (req: MyRequest, res: Response) => {
	const { id } = req.params

	const removeAvatarUsuarioService = new RemoveAvatarUsuarioService()

	await removeAvatarUsuarioService.execute({
		id: parseInt(id),bucket: 'miaudota/avatars'
	})

	return res.status(200).json({sucesso: true})
})

export default usuariosRoutes
