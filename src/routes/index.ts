import { Router } from 'express'
import altenticacaoRoutes from './autenticacao.routes'
import VerificaAutenticacao from '../middlewares/VerificaAutenticacao'
import usuariosRoutes from './usuarios.routes'
import animaisRoutes from './Animais.routes'

const routes = Router()

routes.use('/Autenticacao', altenticacaoRoutes)

// routes.use(VerificaAutenticacao)

routes.use('/Usuarios', usuariosRoutes)
routes.use('/Animais', animaisRoutes)

routes.use('*', (_req, res) => {
  return res.status(404).json({
    sucesso: false,
    mensagem: 'Endereço não encontrado'
  })
})

export default routes
