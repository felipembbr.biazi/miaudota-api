import { compare } from "bcryptjs"
import { sign } from "jsonwebtoken"
import autenticacao from "../../config/autenticacao"
import MyError from "../../errors"
import Usuario from "../../models/Usuario"
import ValidaEmail from "../../shared/ValidaEmail"
import BuscaUsuarioPorEmailService from "../usuarios/BuscaUsuarioPorEmailService"

interface Request {
  email: string,
  senha: string
}

interface Response {
  usuario: Usuario,
  token: string
}

class AutenticaUsuarioService {
  public async execute({ email,senha }: Request): Promise<Response> {
    if (! email){
      throw new MyError('O e-mail do usuário deve ser informado', 401)
    }

    ValidaEmail(email)

    if (! senha){
      throw new MyError('A senha do usuário deve ser informada', 401)
    }

    const buscaUsuarioPorEmailService = new BuscaUsuarioPorEmailService()
    const usuario = await buscaUsuarioPorEmailService.execute(email)

    if (! usuario){
      throw new MyError('E-mail e/ou senha inválida', 401)
    }

    const usuarioAtivo = usuario.ativo
    if (! usuarioAtivo){
      throw new MyError('O usuário está inativo', 401)
    }
    
    const senhaUsuario = usuario.senha
    const senhaConfere = await compare(senha, senhaUsuario!)
    if (! senhaConfere){
      throw new MyError('E-mail e/ou senha inválido', 401)
    }   

    const { secret, expiresIn } = autenticacao.jwt

    const token = sign({}, secret, {
      subject: usuario.id.toString(),
      expiresIn
    })

    delete usuario.senha

    return {
      usuario,
      token
    }
  }
}

export default AutenticaUsuarioService
