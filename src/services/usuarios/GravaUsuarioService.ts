import { hash } from 'bcryptjs'
import { EntityManager } from 'typeorm';
import { ApiDataSource } from '../../data.source';
import MyError from "../../errors";
import Usuario from "../../models/Usuario";
import ValidaEmail from '../../shared/ValidaEmail';
import GravaArquivoAWSS3Service from '../arquivosawss3/GravaArquivoAWSS3Service';
import BuscaUsuarioPorEmailService from "./BuscaUsuarioPorEmailService";

interface Request {
  nome: string
  tipo?: 'admin' | 'instituicao' | 'pessoa'
  email: string
  senha: string
  endereco: string
  telefone: string
  avatar?: Express.Multer.File | undefined
}

class GravaUsuarioService {
  public async execute({ nome,tipo,endereco,telefone,email,senha,avatar}: Request): Promise<void> {
    if (! nome){
      throw new MyError('O nome do usuário deve ser informado')
    }else if (! tipo){
      throw new MyError('O tipo do usuário deve ser informado')
    } else if (! endereco) {
      throw new MyError('O Endereço de residencia deve ser informado')
    } else if (! telefone) {
      throw new MyError('O telefone deve ser informado')
    }

    const tiposUsuariosValidos = ['admin' , 'instituicao' , 'pessoa']
    if (! tiposUsuariosValidos.includes(tipo)){
      throw new MyError('O tipo do usuário informado é inválido')
    }

    if (! email){
      throw new MyError('O e-mail do usuário deve ser informado')
    }
    
    ValidaEmail(email)

    const buscaUsuarioPorEmailService = new BuscaUsuarioPorEmailService()
    const usuarioJaExiste = await buscaUsuarioPorEmailService.execute(email)

    if (usuarioJaExiste){
      throw new MyError('Já existe um usuário com o e-mail informado')
    }

    if (! senha){
      throw new MyError('A senha do usuário deve ser informada')
    }  

    if(avatar){
      var url = (avatar as any) .location
    }

    if(! avatar){
      let nomeurl = nome.trim().replace(' ','+')
      url = `https://ui-avatars.com/api/?name=${nomeurl}&bold=true`
    }
    
    const senhaCriptografada = await hash(senha,8)

    const usuariosRepository = ApiDataSource.getRepository(Usuario)

    const manager: EntityManager = ApiDataSource.manager

    return await manager.transaction(async transactionalEntityManager => {
      const usuario = usuariosRepository.create({
        ativo: true,
        nome,
        tipo,
        endereco,
        telefone,
        email,
        senha: senhaCriptografada,
        avatar: url,
        created_at: new Date()
      })
  
      await transactionalEntityManager.save(usuario)

      if(avatar){
        const gravaArquivoAWSS3Service = new GravaArquivoAWSS3Service()    
          await gravaArquivoAWSS3Service.execute({
            arquivo: avatar
          })
      }
  
      // return usuario
    })
   
  }
}

export default GravaUsuarioService
