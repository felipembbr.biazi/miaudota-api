import { ApiDataSource } from '../../data.source'
import MyError from "../../errors"
import Usuario from '../../models/Usuario'

class BuscaUsuarioPorIdService {
  public async execute(id: number): Promise<Usuario> {
    if (! id){
      throw new MyError('A Id do usuário deve ser informada')
    }

    const usuariosRepository = ApiDataSource.getRepository(Usuario)
    const usuario = await usuariosRepository.findOne({
      where: {
        id
      }
    })

    if (! usuario){
      throw new MyError('Usuário não encontrado com a Id informada', 404)
    }

    return usuario
  }
}

export default BuscaUsuarioPorIdService
