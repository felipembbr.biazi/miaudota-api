import { ApiDataSource } from "../../data.source"
import MyError from "../../errors"
import Usuario from "../../models/Usuario"

interface Request {
  id: number,
  nome: string,
  tipo?: 'admin' | 'instituicao' | 'pessoa',
  telefone?: string,
  endereco?: string,
  usuario: Usuario | undefined
}

class AlteraUsuarioService {
  public async execute({ id,nome,tipo,usuario,telefone,endereco}: Request): Promise<Usuario> {
    if (!id){
      throw new MyError('Id do usuário deve ser informado')
    } else if (!nome){
      throw new MyError('Campo Nome deve ser informado')
    } else if (nome.length<2){
      throw new MyError('Nome deve possuir pelo menos 2 caracteres')
    } else if (!tipo){
      throw new MyError('Campo Tipo deve ser informado')
    } else if (! usuario) {
      throw new MyError('Campo Usuario deve ser informado')
    } else if (! telefone) {
      throw new MyError('O Número de telefone deve ser informado')
    }

    if(id == usuario.id){
      if (tipo !== usuario.tipo){
        throw new MyError('Não é possivel alterar o tipo do seu usuario')
      } 
    }

    const usuariosRepository = ApiDataSource.getRepository(Usuario)
    const usuarioExiste = await usuariosRepository.findOne({
      where: { id }
    })

    if (!usuarioExiste){
      throw new MyError('Usuário não encontrado para alteração', 404)
    }

    const usuarioAltera = usuariosRepository.create({
      id, 
      nome,
      tipo,
      telefone,
      endereco,
      updated_at: new Date()
    })

    await usuariosRepository.save(usuarioAltera)
    
    return usuarioAltera
  }
}

export default AlteraUsuarioService