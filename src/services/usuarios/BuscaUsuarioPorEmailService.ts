import { ApiDataSource } from '../../data.source'
import MyError from "../../errors"
import Usuario from '../../models/Usuario'

class BuscaUsuarioPorEmailService {
  public async execute(email: string): Promise<Usuario | null>{
    if (! email){
      throw new MyError('O e-mail do usuário deve ser informado')
    }

    const usuariosRepository = ApiDataSource.getRepository(Usuario)
    const usuario = await usuariosRepository.findOne({
      where: {
        email
      }
    })

    return usuario
  }
}

export default BuscaUsuarioPorEmailService
