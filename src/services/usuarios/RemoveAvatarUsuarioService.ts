import { ApiDataSource } from '../../data.source'
import MyError from '../../errors'
import ArquivosAWSS3 from '../../models/ArquivosAWSS3'
import BuscaUsuarioPorIdService from './BuscaUsuarioPorIdService'
import RemoveArquivoAWSService from '../arquivosawss3/RemoveArquivoAWSS3Service'
import Usuario from '../../models/Usuario'

interface Request {
  id: number,
  bucket: string
}

class RemoveAvatarUsuarioService {
  public async execute({id, bucket}: Request): Promise<void> {
    if (! id){
      throw new MyError('O id do usuario deve ser infomado')
    }

    const buscaUsuarioPorIdService = new BuscaUsuarioPorIdService()
    const usuario = await buscaUsuarioPorIdService.execute(id)    

    const arquivoRepository = ApiDataSource.getRepository(ArquivosAWSS3)
    const arquivoExiste = await arquivoRepository.findOne({
      where: { 
        url: usuario.avatar 
      }
    })

    if (arquivoExiste){
      const removeArquivoAWSService = new RemoveArquivoAWSService()
      await removeArquivoAWSService.execute({bucket, key: arquivoExiste.key}) 

      await arquivoRepository.delete(arquivoExiste.id)       
    }  
    
    const usuariosRepository = ApiDataSource.getRepository(Usuario)
    const usuarioAltera = usuariosRepository.create({
      id,
      avatar: '',
      updated_at: new Date()
    })
    
    await usuariosRepository.save(usuarioAltera)
  }
}

export default RemoveAvatarUsuarioService
