import { ApiDataSource } from "../../data.source"
import MyError from "../../errors"
import ArquivosAWSS3 from "../../models/ArquivosAWSS3"
import Usuario from "../../models/Usuario"
import GravaArquivoAWSS3Service from "../arquivosawss3/GravaArquivoAWSS3Service"
import RemoveArquivoAWSService from "../arquivosawss3/RemoveArquivoAWSS3Service"
import BuscaUsuarioPorIdService from "./BuscaUsuarioPorIdService"

interface Request{
  id: number,
  imagem: Express.Multer.File | undefined
}

class AlteraAvatarUsuarioService {
  public async execute({ id, imagem}: Request): Promise<String> {
    if(!id){
      throw new MyError('Id do usuário deve ser informado')
    } else if (!imagem) {
      throw new MyError('A imagem do avatar deve ser informada')
    }       
    
    const avatar = (imagem as any).location

    if(avatar){
      const buscaUsuarioPorIdService = new BuscaUsuarioPorIdService()
      const usuario = await buscaUsuarioPorIdService.execute(id) 

      const arquivoRepository = ApiDataSource.getRepository(ArquivosAWSS3)
      const arquivoExiste = await arquivoRepository.findOne({
       where: { 
          url: usuario.avatar 
        }
      })

      if (arquivoExiste){
        const removeArquivoAWSService = new RemoveArquivoAWSService()
        await removeArquivoAWSService.execute({
          bucket: 'miaudota/avatars',
          key: arquivoExiste.key
        }) 

        await arquivoRepository.delete(arquivoExiste.id)       
      }        

      const gravaArquivoAWSS3Service = new GravaArquivoAWSS3Service()    
      await gravaArquivoAWSS3Service.execute({
        arquivo: imagem
      })
    }  
    
    const usuariosRepository = ApiDataSource.getRepository(Usuario)
    const usuarioAltera = usuariosRepository.create({
      id,
      avatar,
      updated_at: new Date()
    })
    
    await usuariosRepository.save(usuarioAltera)
    
    return avatar      
  }
}

export default AlteraAvatarUsuarioService
