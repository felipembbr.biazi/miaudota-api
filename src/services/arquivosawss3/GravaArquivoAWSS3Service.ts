import { ApiDataSource } from '../../data.source';
import MyError from "../../errors";
import ArquivosAWSS3 from '../../models/ArquivosAWSS3';

interface Request {
  arquivo: Express.Multer.File | undefined,
}

class GravaArquivoAWSS3Service {
  public async execute({ arquivo }: Request): Promise<ArquivosAWSS3> {
    if(! arquivo){
      throw new MyError('O arquivo não encontrado',404)
    }

    const url = (arquivo as any).location

    const tipo = (arquivo as any).contentType

    const key = (arquivo as any).key
  

    const arquivosRepository = ApiDataSource.getRepository(ArquivosAWSS3)

    const files = arquivosRepository.create({
      url,
      tipo,
      key,
      created_at: new Date()
    })
   
    await arquivosRepository.save(files)

    return url
  }
}

export default GravaArquivoAWSS3Service