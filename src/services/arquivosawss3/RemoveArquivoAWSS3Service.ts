import awsConfig from '../../config/aws'
import aws from "aws-sdk"
import MyError from '../../errors';

interface Request {
  bucket: string,
  key: string
}

class RemoveArquivoAWSService {
  public async execute({ bucket, key}: Request): Promise<void> {
    if(! bucket){
      throw new MyError('Informe o bucket da AWS')
    } else if (! key){
      throw new MyError('Informe a Key do arquivo da AWS')
    }

    const { accessKeyId,secretAccessKey,region,signatureVersion } = awsConfig
  
    const s3Config = new aws.S3({
      accessKeyId,
      secretAccessKey,
      region,
      signatureVersion
    });
  
    const params = { Bucket: bucket, Key: key }
  
    s3Config.deleteObject(params, (error, data) => {
      if (error){
        throw new MyError('Não foi possivel deletar a imagem no AWS')
      }
    })
  }
}

export default RemoveArquivoAWSService

