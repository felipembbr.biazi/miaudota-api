import { ApiDataSource } from "../../data.source"
import MyError from "../../errors"
import Animal from "../../models/Animal"

interface Request {
  id: number
  nome: string
  status: 'perdido' | 'encontrado' | 'adocao' | 'adotado'
  idade?: number
  sexo: 'macho' | 'femea'
  descricao?: string
}

class AlteraAnimalService {
  public async execute({ id,nome,status,idade,sexo,descricao }: Request): Promise<Animal> {
    if (!id){
      throw new MyError('Id do usuário deve ser informado')
    } else if (!nome){
      throw new MyError('Campo Nome deve ser informado')
    } else if (nome.length<2){
      throw new MyError('Nome deve possuir pelo menos 2 caracteres')
    } else if (! sexo){
      throw new MyError('O sexo do animal deve ser informado')
    } else if (! status) {
      throw new MyError('O Status do animal ser informado')
    }
    
    const animaisRepository = ApiDataSource.getRepository(Animal)
    const animalExiste = await animaisRepository.findOne({
      where: { id }
    })

    if (!animalExiste){
      throw new MyError('Animal não encontrado para alteração', 404)
    }

    const statusAnimalValidos = ['perdido' , 'encontrado' , 'adocao' , 'adotado']
    if (! statusAnimalValidos.includes(status)){
      throw new MyError('O Status do animal informado é inválido')
    }

    const sexoAnimalValidos = ['macho' , 'femea' ]
    if (! sexoAnimalValidos.includes(sexo)){
      throw new MyError('O Sexo do animal informado é inválido')
    }
  
    const animalAltera = animaisRepository.create({
      id, 
      nome,
      idade,
      status,
      sexo,
      descricao,
      updated_at: new Date()
    })

    await animaisRepository.save(animalAltera)
    
    return animalAltera
  }
}

export default AlteraAnimalService