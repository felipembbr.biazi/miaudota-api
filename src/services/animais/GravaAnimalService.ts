import { hash } from 'bcryptjs'
import { EntityManager } from 'typeorm';
import { ApiDataSource } from '../../data.source';
import MyError from "../../errors";
import Animal from "../../models/Animal";
import GravaArquivoAWSS3Service from '../arquivosawss3/GravaArquivoAWSS3Service';

interface Request {
  nome: string
  status: 'perdido' | 'encontrado' | 'adocao' | 'adotado'
  idade?: number
  sexo: 'macho' | 'femea'
  descricao?: string
  avatar?: Express.Multer.File | undefined
}

class GravaAnimalService {
  public async execute({ nome,status,idade,avatar,sexo,descricao }: Request): Promise<Animal> {
    if (! nome){
      throw new MyError('O nome do usuário deve ser informado')
    }else if (! sexo){
      throw new MyError('O sexo do animal deve ser informado')
    } else if (! status) {
      throw new MyError('O Status do animal ser informado')
    }

    const statusAnimalValidos = ['perdido' , 'encontrado' , 'adocao' , 'adotado']
    if (! statusAnimalValidos.includes(status)){
      throw new MyError('O Status do animal informado é inválido')
    }

    const sexoAnimalValidos = ['macho' , 'femea' ]
    if (! sexoAnimalValidos.includes(sexo)){
      throw new MyError('O Sexo do animal informado é inválido')
    }

    // const buscaUsuarioPorEmailService = new BuscaUsuarioPorEmailService()
    // const usuarioJaExiste = await buscaUsuarioPorEmailService.execute(email)

    // if (usuarioJaExiste){
    //   throw new MyError('Já existe um usuário com o e-mail informado')
    // }

    if(avatar){
      var url = (avatar as any) .location
    }

    const animaisRepository = ApiDataSource.getRepository(Animal)

    const manager: EntityManager = ApiDataSource.manager

    return await manager.transaction(async transactionalEntityManager => {
      const animal = animaisRepository.create({
        nome,
        status,
        sexo,
        idade,
        descricao,
        avatar: url,
        id_usuario: 1,
        created_at: new Date()
      })
  
      await transactionalEntityManager.save(animal)

      if(avatar){
        const gravaArquivoAWSS3Service = new GravaArquivoAWSS3Service()    
          await gravaArquivoAWSS3Service.execute({
            arquivo: avatar
          })
      }
  
      return animal
    })
   
  }
}

export default GravaAnimalService
