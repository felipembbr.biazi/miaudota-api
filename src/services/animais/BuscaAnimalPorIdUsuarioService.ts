import { ApiDataSource } from "../../data.source";
import MyError from "../../errors";
import Animal from "../../models/Animal";

class BuscaAnimalPorIdUsuarioService {
  public async execute(id_usuario: number): Promise<Animal[]>{
    if(! id_usuario){
      throw new MyError('Id do cliente deve ser informado')
    }

    const filiaisRepository = ApiDataSource.getRepository(Animal)
    const animais = await filiaisRepository.find({
      where: {
        id_usuario
      }
    }) 

    return animais
  }
}

export default BuscaAnimalPorIdUsuarioService