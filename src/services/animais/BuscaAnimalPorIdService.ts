import { ApiDataSource } from '../../data.source'
import MyError from "../../errors"
import Animal from '../../models/Animal'

class BuscaAnimalPorIdService {
  public async execute(id: number): Promise<Animal> {
    if (! id){
      throw new MyError('A Id do usuário deve ser informada')
    }

    const animaisRepository = ApiDataSource.getRepository(Animal)
    const animal = await animaisRepository.findOne({
      where: {
        id
      }
    })

    if (! animal){
      throw new MyError('Animal não encontrado com a Id informada', 404)
    }

    return animal
  }
}

export default BuscaAnimalPorIdService
