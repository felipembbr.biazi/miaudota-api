import { ApiDataSource } from "../../data.source";
import Animal from "../../models/Animal";

class BuscaAnimaisService {
  public async execute(): Promise<Animal[]> {
    const usuariosRepository = ApiDataSource.getRepository(Animal)
    const animais = await usuariosRepository.find()

    return animais
  }
}

export default BuscaAnimaisService
