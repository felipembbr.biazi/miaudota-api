import { MigrationInterface, QueryRunner, Table } from "typeorm"

export class CriaTabelaArquivosAwsS31670160438031 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.createTable(
          new Table({
            name: 'arquivosawss3',
            columns: [
              {
                name: 'id',
                type: 'int',
                isPrimary: true,
                isGenerated: true,
                generationStrategy: 'increment'
              },
              {
                name: 'url',
                type: 'varchar'
              },
              {
                name: 'key',
                type: 'varchar'
              },
              {
                name: 'tipo',
                type: 'varchar'
              },
              {
                name: 'created_at',
                type: 'timestamp'
              },
              {
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true
              } 
            ]
          })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.dropTable('arquivosawss3')
    }

}
