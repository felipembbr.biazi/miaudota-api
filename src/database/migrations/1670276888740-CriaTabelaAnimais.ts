import { MigrationInterface, QueryRunner, Table } from "typeorm"

export class CriaTabelaAnimais1670276888740 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.createTable(
        new Table({
          name:'animais',
          columns: [
            {
              name: 'id',
              type: 'int',
              isPrimary: true,
              isGenerated: true,
              generationStrategy: 'increment'
            },
            {
              name: 'avatar',
              type: 'varchar',
              isNullable: true
            },
            {
              name: 'id_usuario',
              type: 'int'
            },
            {
              name: 'nome',
              type: 'varchar',
              isNullable: false
            },
            {
              name: 'idade',
              type: 'int',
              isNullable: true
            },
            {
              name: 'sexo',
              type: 'enum',
              enum: ['macho','femea'],
              isNullable: false
            },
            {
              name: 'status',
              type: 'enum',
              enum: ['perdido','encontrado','adocao','adotado'],
              isNullable: false
            },
            {
              name: 'descricao',
              type:  'varchar'
            },
            {
              name: 'created_at',
              type: 'timestamp'
            },
            {
              name: 'updated_at',
              type: 'timestamp',
              isNullable: true
            }
          ]
        })
      )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.dropTable('animais')
    }

}
