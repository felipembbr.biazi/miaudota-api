import { MigrationInterface, QueryRunner, Table } from "typeorm"
import GravaUsuarioService from "../../services/usuarios/GravaUsuarioService"

export class CriaTabelaUsuarios1670026003302 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.createTable(
          new Table({
            name: 'usuarios',
            columns: [
              {
                name: 'id',
                type: 'int',
                isPrimary: true,
                isGenerated: true,
                generationStrategy: 'increment'
              },
              {
                name: 'nome',
                type: 'varchar',
                isNullable: false
              },
              {
                name: 'tipo',
                type: 'enum',
                enum: ['admin','instituicao','pessoa'],
                isNullable: true
              },
              {
                name: 'email',
                type: 'varchar',
                isNullable: false
              },
              {
                name: 'senha',
                type: 'varchar',
                isNullable: false
              },
              {
                name: 'endereco',
                type: 'varchar',
                isNullable: false
              },
              {
                name: 'telefone',
                type: 'varchar',
                isNullable: false
              },
              {
                name: 'ativo',
                type: 'boolean'
              },
              {
                name: 'avatar',
                type: 'varchar',
                isNullable: true
              },
              {
                name: 'created_at',
                type: 'timestamp'
              },
              {
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true
              }
            ]
          })
        )
    
        const gravaUsuarioService = new GravaUsuarioService()
        await gravaUsuarioService.execute({
          nome: 'admin',
          tipo: 'admin',
          email: 'teste@teste.com.br',
          senha: '[1111]',
          endereco: 'endereço teste',
          telefone: "331145453"
        })
        }
    
    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.dropTable('usuarios')
    }

}
