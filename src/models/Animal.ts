import { Column, CreateDateColumn, Entity, JoinColumn, ManyToMany, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import Usuario from "./Usuario";
export type TipoStatus = 'perdido' | 'encontrado' | 'adocao' | 'adotado'
export type TipoSexo = 'macho' | 'femea'

@Entity('animais')
class Animal {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  nome: string

  @Column()
  avatar?: string

  @Column({
    type: 'enum',
    enum: ['perdido','encontrado','adocao','adotado']
  })
  status: TipoStatus

  @Column()
  idade?: number

  @Column({
    type: 'enum',
    enum: ['macho','femea']
  })
  sexo: TipoSexo

  @Column()
  id_usuario: number

  @ManyToOne(()=> Usuario, usuario => usuario.animal)
  @JoinColumn({name: 'id_usuario'})
  usuario: Usuario

  @Column()
  descricao?: string

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default Animal
