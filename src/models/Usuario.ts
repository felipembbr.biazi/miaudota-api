import { Column, CreateDateColumn, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import Animal from "./Animal";

export type TiposUsuario = 'admin' | 'instituicao' | 'pessoa'

@Entity('usuarios')
class Usuario {
  @PrimaryGeneratedColumn()
  id: number

  @OneToMany(() => Animal, animal => animal.usuario)
  @JoinColumn({name: 'id'})
  animal: Animal[]

  @Column()
  nome: string

  @Column({
    type: 'enum',
    enum: ['admin','instituicao','pessoa']
  })
  tipo: TiposUsuario

  @Column()
  ativo: boolean

  @Column()
  email: string

  @Column()
  senha?: string

  @Column()
  endereco: string

  @Column()
  telefone: string

  @Column()
  avatar: string

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default Usuario
