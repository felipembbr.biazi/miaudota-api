import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn, } from "typeorm";

@Entity('arquivosawss3')
class ArquivosAWSS3 {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  url: string

  @Column()
  key: string

  @Column()
  tipo: string

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default ArquivosAWSS3