import { Request,Response,NextFunction } from "express";
import MyError from "../errors";

export default function VerificaErros(erro: Error, _request: Request, response: Response, _next: NextFunction){
  if (erro instanceof MyError){
    return response.status(erro.codigo).json({
      sucesso: false,
      mensagem: erro.mensagem
    })
  }

  console.log(erro)

  return response.status(500).json({
    sucesso: false,
    mensagem: 'Não foi possível processar a requisição. Por favor entre em contato com o administrador do sistema'
  })
}