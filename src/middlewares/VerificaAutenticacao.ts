import { Response,NextFunction } from 'express'
import { decode, verify } from 'jsonwebtoken'
import autenticacao from '../config/autenticacao';
import MyError from '../errors';
import BuscaUsuarioPorIdService from '../services/usuarios/BuscaUsuarioPorIdService';
import MyRequest from "../shared/MyRequest";

export default async function VerificaAutenticacao(req: MyRequest, res: Response, next: NextFunction){
  let authHeader = req.headers.authorization
  if (! authHeader){
    throw new MyError('Token não encontrado', 401)
  }
  if (typeof authHeader !== 'string') {
    authHeader = authHeader[0]
  }

  const [, token] = authHeader?.split(' ')

  try {
    verify(token, autenticacao.jwt.secret)

    const payload = decode(token)

    const userId = Math.floor(Number(payload?.sub))

    const buscaUsuarioPorIdService = new BuscaUsuarioPorIdService()
    const usuario = await buscaUsuarioPorIdService.execute(userId)

    const usuarioAtivo = usuario.ativo
    if (! usuarioAtivo){
      throw new MyError('O usuário está inativo, 401')
    }

    delete usuario.senha

    req.usuario = usuario
   
    return next()
  } catch (erro){
    throw new MyError('Token inválido ou não informado')
  }
}