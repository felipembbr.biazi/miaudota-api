import 'reflect-metadata'
import express from 'express'
import 'express-async-errors'
import './database'
import cors from 'cors'
import VerificaErros from './middlewares/VerificaErros'
import routes from './routes'

const app = express()
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cors())

app.use(routes)
app.use(VerificaErros)

const porta = 3000

app.listen(porta, () => {
	console.log('Servidor iniciado na porta '+porta+'!')
})